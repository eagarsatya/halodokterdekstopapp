﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blessings.Model
{
    class ObatModel
    {
        public int ObatId { get; set; }
        public string ObatName { get; set; }
        public string ObatExpired { get; set; }
        public string ObatDescr { get; set; }
    }
}
