﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blessings.Model
{
    class DokterModel
    {
        public int DokterId { get; set; }
        public string DokterName { get; set; }
        public string DokterSpesialis { get; set; }

    }
}
