﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blessings.Model
{
    class PasienModel
    {
        public int PasienId { get; set; }
        public string PasienName { get; set; }
        public string PenyakitPasien { get; set; }
        public int? NomorTelpon { get; set; }
        public string NamaKerabat { get; set; }
        public int? NoTelponKerabat { get; set; }
    }
}
