﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blessings.Model
{
    class TransactionModel
    {
        public int PasienId { get; set; }
        public int DokterId { get; set; }
        public int ObatId { get; set; }
        public string ReminderTitle { get; set; }
        public string ReminderTime { get; set; }
        public int? KonsumsiObatStock { get; set; }
        public string KonsumsiObatTime { get; set; }
        public int? KonsumsiObatDosis { get; set; }
    }

    public class TransactionObatModel
    {
        public int DokterId { get; set; }
        public string DokterName { get; set; }
        public int ObatId { get; set; }
        public string ObatName { get; set; }
        public string ObatExpired { get; set; }
        public string KonsumsiObatTime { get; set; }
        public int? KonsumsiObatDosis { get; set; }

    }

    public class PasienObatModel
    {
        public int PasienId { get; set; }
        public string PasienName { get; set; }
        public List<TransactionObatModel> ListTransaction { get; set; }
    }
}
