﻿using Blessings.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Blessings.UI
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {

        private static IHttpClientFactory httpClientFactory;

        public Window2()
        {
            InitializeComponent();
            RegisterService();
        }

        private void RegisterService()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();

            var buildService = services.BuildServiceProvider();
            httpClientFactory = buildService.GetRequiredService<IHttpClientFactory>();
        }

        private async void BtnSubmitObat_Click_1(object sender, RoutedEventArgs e)
        {
            string namaObat = tbNamaObat.Text;
            string expiredDate = tbEx.Text;
            string obatDesc = tbNamaObat.Text;

            var empty = string.IsNullOrEmpty(namaObat) == true || string.IsNullOrEmpty(expiredDate) == true || string.IsNullOrEmpty(obatDesc) == true;

            if (empty)
            {
                //Error
                MessageBox.Show("Setiap Field harus di isi!");
                return;
            }

            var newObat = new ObatModel
            {
                ObatDescr = obatDesc,
                ObatExpired = expiredDate,
                ObatName = namaObat
            };

            var json = new StringContent(JsonConvert.SerializeObject(newObat), Encoding.UTF8, "application/json");

            var client = httpClientFactory.CreateClient();

            var response = await client.PostAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Obat/insert-obat", json);

            if (response.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal Input Data");
                //Error
            }

            MessageBox.Show("Sukse Input Data");

            NavigationService navService = NavigationService.GetNavigationService(this);
            MainWindow nextPage = new MainWindow();
            nextPage.Show();
            this.Close();

            return;

        }
    }
}
