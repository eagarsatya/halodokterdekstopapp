﻿using Blessings.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Blessings.UI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private static IHttpClientFactory httpClientFactory;

        public Window1()
        {
            InitializeComponent();
            RegisterService();
        }

        private void RegisterService()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();

            var buildService = services.BuildServiceProvider();
            httpClientFactory = buildService.GetRequiredService<IHttpClientFactory>();
        }

        private async void Btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            string namaPasien = tbNama.Text;
            try
            {
                int.TryParse(tbNoTelp.Text.ToString(), out int b);
            }
            catch
            {
                //Error
                MessageBox.Show("No Telp harus angka!");
                return;
            }
            string namaKerabatPasien = tbNK.Text;

            try
            {
                int.TryParse(tbNTK.Text.ToString(), out int a);
            }
            catch
            {
                //Error
                MessageBox.Show("No Telp Kerabat harus angka!");
                return;
            }

            int.TryParse(tbNoTelp.Text.ToString(), out int telpPasien);
            int.TryParse(tbNTK.Text.ToString(), out int teleponKerabatPasien);

            string riwayatPenyakitPasien = tbRsakit.Text;

            var valid = string.IsNullOrEmpty(namaPasien) == true || string.IsNullOrEmpty(namaKerabatPasien) == true || string.IsNullOrEmpty(riwayatPenyakitPasien) == true;

            if (valid)
            {
                MessageBox.Show("Setiap data harus di isi!");
                return;
            }

            var newPasien = new PasienModel
            {
                NamaKerabat = namaKerabatPasien,
                NomorTelpon = telpPasien,
                NoTelponKerabat = teleponKerabatPasien,
                PasienName = namaPasien,
                PenyakitPasien = riwayatPenyakitPasien
            };

            var json = new StringContent(JsonConvert.SerializeObject(newPasien), Encoding.UTF8, "application/json");

            var client = httpClientFactory.CreateClient();

            var response = await client.PostAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Pasien/insert-pasien", json);

            if (response.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal Input Data");
                //Error
            }

            MessageBox.Show("Sukses Input Data");

            NavigationService navService = NavigationService.GetNavigationService(this);
            MainWindow nextPage = new MainWindow();
            nextPage.Show();
            this.Close();

            return;
        }
    }
}
