﻿using Blessings.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Blessings.UI
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        public Window3()
        {
            InitializeComponent();
            RegisterService();
        }

        private static IHttpClientFactory httpClientFactory;

        private void RegisterService()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();

            var buildService = services.BuildServiceProvider();
            httpClientFactory = buildService.GetRequiredService<IHttpClientFactory>();
        }

        private async void BtnCari_Click(object sender, RoutedEventArgs e)
        {
            string namaPasien = tbNamaPasien.Text;

            if (string.IsNullOrEmpty(namaPasien) == true)
            {
                //Error
                MessageBox.Show("Nama pasien harus di isi!");
                return;
            }

            var client = httpClientFactory.CreateClient();
            var respond = await client.GetAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Transaction/get-transaction-by-pasien-name/" + namaPasien);

            if (respond.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal memperoleh data");
                return;
            }

            var contentJson = await respond.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeObject<PasienObatModel>(contentJson);

            var dataObat = content.ListTransaction;

            DataGridObat.ItemsSource = dataObat;

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
