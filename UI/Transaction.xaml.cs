﻿using Blessings.Model;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Navigation;

namespace Blessings.UI
{
    /// <summary>
    /// Interaction logic for Transaction.xaml
    /// </summary>
    public partial class Transaction : Window
    {
        private static IHttpClientFactory httpClientFactory;

        public Transaction()
        {
            InitializeComponent();
            RegisterService();
        }

        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string namaPasien = tbPasien.Text;
            string namaDokter = tbDokter.Text;
            string namaObat = tbObat.Text;
            string reminderTitle = tbTitle.Text;
            string reminderTime = tbRTime.Text;
            int stok;
            try
            {
                stok = int.Parse(tbStock.Text);
            }
            catch
            {
                MessageBox.Show("Stok harus angka!");
                return;
            }

            int dosis;
            try
            {
                MessageBox.Show("Dosis harus angka!");
                dosis = int.Parse(tbDosis.Text);
            }
            catch
            {
                return;
            }

            string konsumsiObat = tbKonsumsi.Text;

            var valid = string.IsNullOrEmpty(namaPasien) == true || string.IsNullOrEmpty(namaDokter) == true || string.IsNullOrEmpty(namaObat) == true || string.IsNullOrEmpty(reminderTitle) == true || string.IsNullOrEmpty(reminderTime);

            if (valid)
            {
                MessageBox.Show("Data harus terisi");
                return;
            }

            //cari id pasien,
            var client = httpClientFactory.CreateClient();
            var respond = await client.GetAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Pasien/get-patient-by-name/" + namaPasien);

            if (respond.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal mencari nama pasien!");
                return;
            }

            var contentJson = await respond.Content.ReadAsStringAsync();
            var pasien = JsonConvert.DeserializeObject<PasienModel>(contentJson);

            if (pasien == null || pasien.PasienId == 0)
            {
                MessageBox.Show("Gagal mencari nama pasien!");
                return;
            }

            var pasienId = pasien.PasienId;

            //cari id dokter,
            respond = await client.GetAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Dokter/get-dokter-by-name/" + namaDokter);


            if (respond.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal mencari nama dokter!");
                return;
            }

            contentJson = await respond.Content.ReadAsStringAsync();
            var dokter = JsonConvert.DeserializeObject<DokterModel>(contentJson);

            if (dokter == null || dokter.DokterId == 0)
            {
                MessageBox.Show("Gagal mencari nama dokter!");
                return;
            }

            var dokterId = dokter.DokterId;

            //cari id obat,
            respond = await client.GetAsync("https://halodokterbackend20200104055434.azurewebsites.net/api/Obat/get-obat-by-name/" + namaObat);

            if (respond.IsSuccessStatusCode == false)
            {
                MessageBox.Show("Gagal mencari nama obat!");
                return;
            }

            contentJson = await respond.Content.ReadAsStringAsync();

            var obat = JsonConvert.DeserializeObject<ObatModel>(contentJson);

            if (obat == null || obat.ObatId == 0)
            {
                MessageBox.Show("Gagal mencari nama obat!");
                return;
            }
            var obatId = obat.ObatId;


            var newTransaction = new TransactionModel
            {
                DokterId = dokterId,
                ObatId = obatId,
                KonsumsiObatDosis = dosis,
                KonsumsiObatStock = stok,
                KonsumsiObatTime = konsumsiObat,
                PasienId = pasienId,
                ReminderTime = reminderTime,
                ReminderTitle = reminderTitle
            };

            var json = new StringContent(JsonConvert.SerializeObject(newTransaction), Encoding.UTF8, "application/json");

            var response = await client.PostAsync("https://halodokterbackend20200104055434.azurewebsites.net//api/Transaction/insert-transaction", json);

            if (response.IsSuccessStatusCode == false)
            {
                //Error
                MessageBox.Show("Gagal input data!");
                return;
            }

            MessageBox.Show("Sukses input data!");

            NavigationService navService = NavigationService.GetNavigationService(this);
            MainWindow nextPage = new MainWindow();
            nextPage.Show();
            this.Close();

            return;

        }

        private void RegisterService()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();

            var buildService = services.BuildServiceProvider();
            httpClientFactory = buildService.GetRequiredService<IHttpClientFactory>();
        }
    }
}
